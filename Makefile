build: test 
	# build
	go build -o bin/ediff ./cmd

test: fmt
	# test
	go test --cover ./...
fmt: 
	gofmt -s -w .