package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"time"

	"gitlab.com/efronlicht/ediff"
)

var n = flag.Int("n", 1, "number of times to differentiate")

func main() {
	flag.Parse()
	go func() {
		time.Sleep(5 * time.Second)
		log.Fatal("expected input from stdin")
	}()
	b, err := io.ReadAll(os.Stdin)
	if err != nil {
		log.Fatal("failed to read stdin", err)
	}
	terms, err := ediff.Parse(string(b))
	if err != nil {
		log.Fatal(err)
	}
	for i := *n; i > 0; i-- {
		terms = ediff.Diff(terms)
	}
	if len(terms) == 0 {
		fmt.Println("0")
		return
	}
	for i := range terms {
		fmt.Print(terms[i], " ")
	}
	fmt.Println("")
}
