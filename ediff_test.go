package ediff

import "testing"

func TestDiff(t *testing.T) {
	for _, tt := range []struct {
		in      string
		want    []Term
		wantErr bool
	}{
		{in: "0x^3"},
		{in: "x", want: []Term{{a: 1}}},
		{in: "-2x^-3", want: []Term{{a: 6, n: -4}}},
		{in: "-2x^1 + 1x^1 + 1x^1 +   3x^2", want: []Term{{a: 6, n: 1}}},
		{in: "3", want: nil},
	} {

		t.Logf("%+v", tt)
		if parsed, err := Parse(tt.in); (err != nil) != tt.wantErr {
			t.Fatalf("expected err: %v, but err was %v", tt.wantErr, err)
		} else if got := Diff(parsed); !sliceEq(got, tt.want) {
			t.Errorf("expected clean(%+v)=%+v, but got %+v", tt.in, tt.want, got)
		}

	}
}

func TestParse(t *testing.T) {
	for _, tt := range []struct {
		in      string
		want    []Term
		wantErr bool
	}{
		{in: "0x^3"},
		{in: "-2x^-3", want: []Term{{a: -2, n: -3}}},
		{in: "-2x^1 + 1x^1 + 1x^1 +   3x^2", want: []Term{{a: 3, n: 2}}},
		{in: "-asndm,asdn", wantErr: true},
		{in: "2", want: []Term{{a: 2}}},
		{in: "NaN+x^3", wantErr: true},
		{in: "x", want: []Term{{a: 1, n: 1}}},
	} {

		t.Logf("%+v", tt)
		if got, err := Parse(tt.in); (err != nil) != tt.wantErr {
			t.Fatalf("expected err: %v, but err was %v", tt.wantErr, err)
		} else if !sliceEq(got, tt.want) {
			t.Errorf("expected clean(%+v)=%+v, but got %+v", tt.in, tt.want, got)
		}

	}
}

func TestClean(t *testing.T) {
	for _, tt := range []struct {
		in, want []Term
	}{
		{
			in: []Term{{a: 0, n: 0}},
		},
		{
			in:   []Term{{a: 1, n: 1}, {a: 1, n: 1}, {a: 2, n: 2}},
			want: []Term{{a: 2, n: 1}, {a: 2, n: 2}},
		},
	} {
		t.Logf("%+v", tt)
		if got := clean(copySlice(tt.in)); !sliceEq(got, tt.want) {
			t.Errorf("expected clean(%+v)=%+v, but got %+v", tt.in, tt.want, got)
		}

	}
}

func copySlice[T any](src []T) []T {
	dst := make([]T, len(src))
	copy(dst, src)
	return dst
}

func sliceEq[T comparable](a, b []T) bool {
	if len(a) != len(b) {
		return false
	}
	for i := range a {
		if a[i] != b[i] {
			return false
		}
	}
	return true
}
