package ediff

import (
	"fmt"
	"log"
	"math"
	"sort"
	"strconv"
	"strings"
)

type Term struct{ a, n float64 }

func (t Term) String() string {
	switch t.n {
	case 0:
		return fmt.Sprintf("%g", t.a)
	case 1:
		return fmt.Sprintf("%gx", t.a)
	default:
		return fmt.Sprintf("%gx^%g", t.a, t.n)
	}
}

// differentiate the terms, dropping them if they become zero. this modifies the slice in place.
func Diff(terms []Term) []Term {
	out := terms[:0]
	for _, t := range terms {
		if t.n == 0 {
			continue
		}
		out = append(out, Term{a: t.a * t.n, n: t.n - 1})

	}
	return out
}

// transform statements like "3+   2x ^ 3  - 7x" => 3 +2x^3 -7x
var bindright = strings.NewReplacer("+ ", "+", " ^ ", "^", "- ", "-", "  ", " ", "\n", " ", "\t", " ")

// Parse polynomial expressions of x
func Parse(s string) (terms []Term, err error) {
	var old string
	for {
		old, s = s, bindright.Replace(s)
		if old == s {
			s = strings.TrimSpace(s)
			break
		}
	}
	fields := strings.Fields(s)
	terms = make([]Term, len(fields))
	for i, f := range fields {
		rawA, rawN, ok := strings.Cut(f, "x^") // usual case: ax^n
		if ok {
			goto ATOF
		}
		// omitted pow: "ax"
		if rawA, rawN, ok = strings.Cut(f, "x"); ok && rawN == "" {
			rawN = "1"
		} else { // omitted x: "a"
			rawN = "0"
		}
	ATOF:
		if rawA == "" {
			rawA = "1"
		}
		if terms[i].a, err = strconv.ParseFloat(rawA, 64); err != nil {
			return nil, fmt.Errorf("failed to parse coefficient of subexpr %q: %w", f, err)
		}
		if math.IsNaN(terms[i].a) || math.IsInf(terms[i].a, 0) {
			return nil, fmt.Errorf("failed to parse coefficient of subexpr %q: NaN or INF", f)
		}

		if terms[i].n, err = strconv.ParseFloat(rawN, 64); err != nil {
			return nil, fmt.Errorf("failed to parse exponent of subexpr %q: %w", f, err)
		}
		if math.IsNaN(terms[i].n) || math.IsInf(terms[i].n, 0) {
			return nil, fmt.Errorf("failed to parse exponent of subexpr %q: NaN or INF", f)
		}
	}
	return clean(terms), nil
}

func clean(terms []Term) []Term {
	less := func(i, j int) bool { return terms[i].n < terms[j].n }
	swapRemove := func(i int) {
		if len(terms) == 1 {
			terms = nil
			return
		}
		terms[i], terms[len(terms)-1] = terms[len(terms)-1], terms[i]
		terms = terms[:len(terms)-1]
	}
	sort.Slice(terms, less)
	startingLen := len(terms)
	// combine like terms
	for i := len(terms) - 1; i > 0; i-- {
		if terms[i].n == terms[i-1].n {
			log.Printf("match %d: %v, %v", i, terms[i], terms[i-1])
			terms[i-1].a += terms[i].a
			swapRemove(i)
		}
	}
	// cancellation
	for i := len(terms) - 1; i >= 0; i-- {
		if terms[i].a == 0 {
			swapRemove(i)
		}
	}
	if startingLen != len(terms) { // we swap-removed, so they're out of order. resort.
		sort.Slice(terms, less)
	}
	return terms
}
